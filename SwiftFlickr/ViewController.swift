//
//  ViewController.swift
//  SwiftFlickr
//
//  Created by Douglas Sass on 3/3/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var SearchTxtFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let pcvc = storyboard?.instantiateViewController(withIdentifier: "PicCollectionViewController") as! PicCollectionViewController
        pcvc.searchTerm = SearchTxtFld.text!
        navigationController?.pushViewController(pcvc, animated: true)
    }


}

