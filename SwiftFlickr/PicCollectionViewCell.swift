//
//  PicCollectionViewCell.swift
//  SwiftFlickr
//
//  Created by Douglas Sass on 3/3/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit

class PicCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var pictureImgVw: UIImageView!
    @IBOutlet weak var pictureLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
