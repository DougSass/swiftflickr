//
//  PicCollectionViewController.swift
//  SwiftFlickr
//
//  Created by Douglas Sass on 3/3/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit

class PicCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    var searchTerm = ""
    
    @IBOutlet weak var picCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.picCollectionView.register(UINib(nibName: "PicCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        let url = flickrSearchURLForSearchTerm(searchTerm)
        
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            print(json)
        }
        
        task.resume()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! PicCollectionViewCell
        
        //How to get the image = http://www.flickr.com/services/api/misc.urls.html
        
        cell.pictureImgVw.backgroundColor = UIColor.blue
        cell.pictureLbl.backgroundColor = UIColor.green
        cell.pictureLbl.text = "CHANGE"
        // Configure the cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 40, height: self.view.frame.size.width/2 - 40)
    }
    
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=25&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }


}
